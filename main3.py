import asyncio
import sys

import sqlite3



def worker(db):
    cur



def main():
    con = sqlite3.connect('example.db')
    cur = con.cursor()

    cur.execute('''
                CREATE TABLE IF NOT EXISTS proxies (
                   id integer primary key autoincrement,
                   type int not null,
                   address text not null, 
                   location text null, 
                   prio int default 100 not null,
                   down bool default false not null
                )''')

    cur.execute("INSERT INTO  proxies (type, address) VALUES "
                "(5, '185.226.124.11:24488'),"
                "(5, '173.212.220.213:20937'),"
                "(5, '185.200.37.118:10820'),"
                "(5, '128.199.245.23:24527'),"
                "(5, '193.169.4.184:10801'),"
                "(5, '18.233.214.217:9050'),"
                "(5, '72.210.252.137:4145')")

    cur.execute('''CREATE TABLE IF NOT EXISTS targets (
                    id integer primary key autoincrement,
                    protocol text default 'http',
                    address text,
                    domain_name text null
                )''')
    con.commit()

    con.close()


if __name__ == '__main__':
    sys.exit(main())
