import asyncio
import json
import logging.config

import httpx

logging.config.dictConfig({
    "version": 1,
    "formatters": {
        "basic": {
            "format": "%(asctime)s:%(levelname)s:%(name)s %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "basic"
        },
        "null": {
            "class": "logging.NullHandler"
        }
    },
    "loggers": {
        "admin": {
            "level": "INFO",
            "handlers": ["console"]
        }
    }
})

log = logging.getLogger("admin")


async def request(client, url):
    try:
        resp = await client.get(url)
        return url, resp
    except httpx.ReadTimeout:
        log.warning(f"type 5 proxy provider '{url}' read time out")
        return url, None

SPYSME = 'https://spys.me/socks.txt'  # 78.140.37.205:1080 RU-H - \n
OPENPROXY = 'https://openproxy.space/list/socks5'
# <link data-n-head="ssr" data-hid="i18n-alt-ru" rel="alternate" href="/ru/list/socks5" hreflang="ru">
# {code:"RU",count:10,items:["37.200.66.166:9051","195.93.173.58:9050","188.120.245.247:12432","195.149.196.135:1080","31.25.243.40:9582","37.131.202.95:33427","185.32.120.242:1080","212.103.119.24:1080","85.159.44.163:9050","94.159.31.98:1080"],active:a}


async def main():
    with open('providers.json') as f:
        proxy_providers = json.load(f)['proxy-providers']
    type5_provider_urls = [p['url'] for p in proxy_providers if p['type'] == 5]

    async with httpx.AsyncClient(timeout=httpx.Timeout(20), follow_redirects=True) as client:

        for url, resp in await asyncio.gather(*[request(client, url) for url in type5_provider_urls]):
            if not resp:
                continue
            if resp.status_code != 200:
                log.warning(f"proxy provider {url} not available: {resp.status_code}")
                continue
            print(url, resp.content)


if __name__ == '__main__':
    asyncio.run(main())
