import asyncio
import json
import logging
import random
import sys

import httpx

# log = logging.getLogger()
# logging.basicConfig(level=logging.WARNING,
#                     format="%(asctime)s:%(levelname)s:%(message)s")


async def work(target, proxy):
    timeout = httpx.Timeout(connect=60, read=10, write=10, pool=10)
    async with httpx.AsyncClient(timeout=timeout, verify=False, proxies=proxy, follow_redirects=False) as client:
        return await client.get(target)


# stream = True to avoid reading response body
# follow_redirects = True


def is_caused_by(exc, root_cause):
    if exc is None:
        return

    if type(exc) == root_cause:
        return exc

    if found_root_cause := is_caused_by(exc.__context__, root_cause):
        return found_root_cause
    else:
        return is_caused_by(exc.__cause__, root_cause)


async def main():
    target = "https://google.com"

    with open("proxies.json") as f:
        proxies = json.load(f)

    socks5_proxies = proxies["5"]
    random.shuffle(socks5_proxies)
    for proxy in socks5_proxies:
        proxy = f"socks5://{proxy}"
        try:
            resp = await work(target, proxy)
            log.warning(f"proxy: {proxy}, {resp.status_code} {resp.elapsed} {resp.headers}")

        except Exception as e:
            # if cause := is_caused_by(e, ConnectionResetError):
            #     sender_log.error(f"proxy: {proxy} - proxy unavailable: {cause}")
            # else:
            log.exception(f"proxy: {proxy}")


if __name__ == '__main__':
    sys.exit(asyncio.run(main()))
